package com.abdou.categoryservice.api.repository;

import com.abdou.categoryservice.api.entity.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategorieRepository extends JpaRepository<Categorie,Long> {
    public Categorie findByLibelle(String libelle);
}
